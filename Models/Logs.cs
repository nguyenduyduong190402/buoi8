﻿using System.ComponentModel.DataAnnotations;

namespace buoi8.Models
{
    public class Logs
    {
        public int LogsId { get; set; }
        [Required, StringLength(100)]
        public decimal TransactionalID { get; set; }
        [Range(0.01, 10000.00)]
        public string Logindate { get; set; }
        public string Logintime { get; set; }
        public int AddTextHere { get; set; }
    }
}
