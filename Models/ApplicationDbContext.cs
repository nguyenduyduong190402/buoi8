﻿using Microsoft.EntityFrameworkCore;

namespace buoi8.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employees> Employeess { get; set; }
        public DbSet<Reports> Reportss { get; set; }

        public DbSet<Transactions> Transactionss { get; set; }
        public DbSet<Logs> Logss { get; set; }
        public DbSet<Accounts> Accountss { get; set; }

    }
}
