﻿using System.ComponentModel.DataAnnotations;

namespace buoi8.Models
{
    public class Accounts
    {
        public int AccountsId { get; set; }
        [Required, StringLength(100)]
        public string CustomerD { get; set; }
        [Range(0.01, 10000.00)]

        public string Accountname { get; set; }
        public int AddTextHere { get; set; }
    }
}
